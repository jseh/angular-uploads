import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { ProbarSubidaComponent } from './probar-subida/probar-subida.component';
import { ProbarSubidaMultipleComponent } from './probar-subida-multiple/probar-subida-multiple.component';
import { UploadFilesService } from './upload-files.service';

@NgModule({
  declarations: [
    AppComponent,
    ProbarSubidaComponent,
    ProbarSubidaMultipleComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    HttpClientModule,
  ],
  providers: [UploadFilesService],
  bootstrap: [AppComponent]
})
export class AppModule { }
