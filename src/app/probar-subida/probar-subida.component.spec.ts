import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProbarSubidaComponent } from './probar-subida.component';

describe('ProbarSubidaComponent', () => {
  let component: ProbarSubidaComponent;
  let fixture: ComponentFixture<ProbarSubidaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProbarSubidaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProbarSubidaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
