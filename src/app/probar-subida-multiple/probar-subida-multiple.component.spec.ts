import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProbarSubidaMultipleComponent } from './probar-subida-multiple.component';

describe('ProbarSubidaMultipleComponent', () => {
  let component: ProbarSubidaMultipleComponent;
  let fixture: ComponentFixture<ProbarSubidaMultipleComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProbarSubidaMultipleComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProbarSubidaMultipleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
