import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProbarSubidaComponent } from './probar-subida/probar-subida.component';
import { ProbarSubidaMultipleComponent } from './probar-subida-multiple/probar-subida-multiple.component';

const routes: Routes = [
  {path: 'subida', component: ProbarSubidaComponent},
  {path: 'subida2', component: ProbarSubidaMultipleComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
